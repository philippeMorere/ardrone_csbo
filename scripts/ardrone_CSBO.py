#!/usr/bin/env python
import rospy
# Finding the path of the current project
import inspect, os
this_project_path = os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))
# Importing the cont_act_pomdp project which should be located at the root of the current project
import sys
sys.path.insert(0, this_project_path + '/cont_act_pomdp')

import roslaunch
import main
import agent
from environment.pose import Pose
from environment.world import World
from tum_ardrone.msg import filter_state
from std_msgs.msg import Float64
from std_msgs.msg import String
from std_msgs.msg import Empty


def send_command(pub, command):
    rospy.loginfo('Sending command %s' % command)
    pub.publish(command)

def send_trajectory(pub, traj_pos):
    rospy.loginfo('Sending new trajectory /%s .' % traj_pos)
    rate = rospy.Rate(10)
    # The minus sign in front of the x coordinate is used to invert the x axis in the simulator.
    for coord in traj_pos:
        send_command(pub, "c goto %s %s 1 0" % (-coord[0], coord[1]))
        rate.sleep()
    # Send last command twice
    send_command(pub, "c goto %s %s 1 0" % (-traj_pos[-1][0], traj_pos[-1][1]))

# This is a blocking call
def gather_pos_obs():
    d={ 'init_queue_size': True,
        'last_queue_size': -1,
        'new_data_pose': False,
        'new_data_obs': False,
        'traj_end': False}
    samp_pos = []
    samp_obs = []

    # Callback for predictedPose topic. Only adds data if a sub-checkpoint was reached
    def callback_pose(data):
        if d['new_data_pose']:
            samp_pos.append([data.x, data.y]) #TODO: Extend to 3D coordinates, data.z])
            rospy.loginfo('New pose gathered %s' % samp_pos[-1])
            d['new_data_pose'] = False

    # Callback for objective function topic. Only adds data if a sub-checkpoint was reached
    def callback_objfn(data):
        if d['new_data_obs']:
            samp_obs.append(data.data)
            rospy.loginfo('New observation gathered %s' % samp_obs[-1])
            d['new_data_obs'] = False

    # Callback for tum_ardrone/com topic. Watches if sub-checkpoints are reached
    def callback_com(data):
        data_str = str(data)
        if data_str.find('(Queue:') == -1:
            # Wrong type of message
            return
        try:
            curr_queue_size = int(data_str.split('(Queue: ')[1].split(')')[0])
        except IndexError:
            rospy.logerr('Error: Couldn\'t parse /tum_ardrone/com queue size. Message:\n %s' % data)
            return

        if d['init_queue_size']:
            d['last_queue_size'] = curr_queue_size
            d['init_queue_size'] = False
        if curr_queue_size < d['last_queue_size']:
            # A checkpoint has been reached
            d['new_data_pose'] = True
            d['new_data_obs'] = True
            d['last_queue_size'] = curr_queue_size
        if d['last_queue_size'] == 0:
            # End of trajectory, stop gathering
            d['traj_end'] = True

    rospy.Subscriber("/ardrone/predictedPose", filter_state, callback_pose)
    rospy.Subscriber("/ardrone_CSBO/objective_fn/noisy", Float64, callback_objfn)
    rospy.Subscriber("/tum_ardrone/com", String, callback_com)

    while not d['traj_end']:
        rospy.sleep(0.5)

    return samp_pos, samp_obs

def get_init_pose():
    d = {'x': 0,'y': 0, 'modified': False}
    def callback_pose(data):
        d['x'] = data.x
        d['y'] = data.y
        d['modified'] = True
        
    rospy.Subscriber("/ardrone/predictedPose", filter_state, callback_pose)
    while not d['modified']:
        rospy.sleep(0.1)
    return d['x'], d['y']

def get_init_sample():
    d = {'sample': 0, 'modified': False}
    def callback_objfn(data):
        d['sample'] = data.data
        d['modified'] = True
        
    rospy.Subscriber("/ardrone_CSBO/objective_fn/noisy", Float64, callback_objfn)
    while not d['modified']:
        rospy.sleep(0.1)
    return d['sample']

def wait_PTAM_ready():
    d = {'ready': False}
    def callback_com(data):
        if d['ready']:
            return
        data_str = str(data)
        if data_str.find('u s PTAM:') == -1:
            # Wrong type of message
            return
        elif data_str.find('PTAM: Idle') != -1 or data_str.find('PTAM: Initializ') != -1: 
            # PTAM not yet completely initialized.
            return
        else:
            d['ready'] = True
            rospy.loginfo('PTAM is initialized.')
    rospy.Subscriber("/tum_ardrone/com", String, callback_com)
    while not d['ready']:
        rospy.sleep(0.01) 

def ardrone_CSBO(*config):
    # Sleep while other nodes start
    rospy.sleep(8.0)

    # Setup publisher
    pub = rospy.Publisher('/tum_ardrone/com', String, queue_size=1)
    pub_takeoff = rospy.Publisher('/ardrone/takeoff', Empty, queue_size=1)
    rospy.init_node('ardrone_CSBO', anonymous=True)
    
    # Sleep a little be to make sure the publisher is registered
    rospy.sleep(0.3)
   
    # Start recording in rosbag
    rosbag_args = '-o {}{} /ardrone_CSBO/objective_fn/ground_trurh /ardrone_CSBO/objective_fn/noisy /ground_truth/state /ardrone/predictedPose /ardrone/imu /ardrone/navdata /tum_ardrone/com'.format(this_project_path, '/rosbags/dump')
    node = roslaunch.core.Node('rosbag', 'record', args=rosbag_args)
    launch = roslaunch.scriptapi.ROSLaunch()
    launch.start()
    rosbag_process = launch.launch(node)
    rospy.loginfo('Recording in rosbag started.')

    # Change mode to autopilot. Important otherwise the other command don't register.
    send_command(pub, "c start")
    rospy.sleep(1.0)

    # Run autoInit routine
    send_command(pub, "c autoInit 500 800 4000 0.5")

    # Wait for autoInit to finish
    wait_PTAM_ready()

    # Initialize world, pose, ...
    init_pos_x, init_pos_y = get_init_pose()
    init_sample = get_init_sample()
    pos = Pose(init_pos_x, init_pos_y, (0, 0), (0, 0))
    obj_fun_type = config[5]
    world = World(pos, obj_fun_type)
    n_steps = config[1]
    acc_rew = [0]
    arr_pos = Pose.to_x_array(pos)
    if obj_fun_type == 'dynamic':
        arr_pos.append(world.time)

    # Create agent and give it a first observation
    agt = agent.Agent(world.simulator, world.init_pos, init_sample, obj_fun_type, config[4],
                      config[2], config[3])

    # The drone is ready to rock
    rospy.loginfo('Ready to go.')

    for i in range(n_steps):
        rospy.loginfo('Step {} of {}'.format(i, n_steps))

        # Agent chooses the next action
        act = agt.select_action(pos)

        # Execute action and collect observations along the trajectory
        traj_pos = world.simulator.sample_array_trajectory(act)
	send_trajectory(pub, traj_pos)

        # Executing the trajectory and waiting until it finishes
	rospy.loginfo('Starting trajectory.')	
	samp_pos, samp_obs = gather_pos_obs()
	rospy.loginfo('Trajectory ended.')	

        # Computing the current pose from the trajecory
        pos = world.simulator.sample_trajectory_at(act, 1).clone()
	pos.x = samp_pos[-1][0]
	pos.y = samp_pos[-1][1]

        rew = acc_rew[-1]
   
	# Giving observations to the agent
        for p, o in zip(samp_pos, samp_obs):
            agt.observe(p, o)
            rew += o
        acc_rew.append(rew)

        # Optimize the belief hyper-parameters every 10 steps
	if (i + 1) % 10 == 0:
            agt.optimize()
    rospy.loginfo('Done.')
    
    # Land
    send_command(pub, "c land")
    rospy.sleep(2.0)

    # Stop recording the rosbag
    rosbag_process.stop()

    return acc_rew

if __name__ == '__main__':
    nb_ep = 5
    run_per_exp = 1
    experiment = (4, nb_ep, 'MTCS_cont', (3, 150, 1), 30, 'static', run_per_exp)
    try:
       rospy.loginfo('Experiment finished with accumulated reward {}.'.format(ardrone_CSBO(*experiment)))
    except rospy.ROSInterruptException:
        pass
