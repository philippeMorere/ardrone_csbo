#!/usr/bin/env python
# license removed for brevity
import rospy
import sys
from tum_ardrone.msg import filter_state
from nav_msgs.msg import Odometry
from std_msgs.msg import String

# This nodes ensures that the drone stays within the delimited flight zone.
# Should the drone get out of the zone (min - tolerance to max + tolerance for each coordinate),
# a clearCommand followed by a goto command is sent to the drone to make it go back
# within the authorized flight area.
def env_limits_node(x_min=0, x_max=5, y_min=0, y_max=5, z_min=1, z_max=5, tolerance=0.5):
    # Setup publisher
    pub = rospy.Publisher('/tum_ardrone/com', String, queue_size=1)
    rospy.init_node('env_limits', anonymous=True)
    
    # Sleep a little be to make sure the publisher is registered
    rospy.sleep(0.3)
    rate = rospy.Rate(10)
    time_between_corrections = 1.0 # 1 sec
    
    # Compute tolerance fractions. Easier to apply on PTAM coordinates
    tol_x = tolerance / (x_max - x_min)
    tol_y = tolerance / (y_max - y_min)
    tol_z = tolerance / (z_max - z_min)
    
    ptam_coords = {'init': True, 'out_of_limits': False, 'corrected_time': -1, 'x': 0.0, 'y': 0.0, 'z':0.0, 'min_x': 0.0, 'max_x': 0.0, 'min_y': 0.0, 'max_y': 0.0, 'min_z': 0.0, 'max_z': 0.0}
    # Callback for ground truth Pose topic. Computes the objective function for each message received here.
    def callback_pose(data):
        position = data.pose.pose.position
        x = float(position.x)
        y = float(position.y)
        z = float(position.z)
        msg = ""
        corrected = False

	# Check that the drone is within the limits, in gazebo coordinates.
	# Correct x
        if x < x_min - tolerance:
            msg = "Drone out of bounds: x<x_min {}".format(x)
            x = ptam_coords['x_min'] + tol_x * (ptam_coords['x_max'] - ptam_coords['x_min'])
            corrected = True
        elif x > x_max + tolerance:
            msg = "Drone out of bounds: x>x_max {}".format(x)
            x = ptam_coords['x_max'] - tol_x * (ptam_coords['x_max'] - ptam_coords['x_min'])
            corrected = True
	else:
	    x = ptam_coords['x']
	
	# Correct y
        if y < y_min - tolerance:
            msg += "Drone out of bounds: y<y_min {}".format(y)
            y = ptam_coords['y_min'] + tol_y * (ptam_coords['y_max'] - ptam_coords['y_min'])
            corrected = True
        elif y > y_max + tolerance:
            msg += "Drone out of bounds: y>y_max {}".format(y)
            y = ptam_coords['y_max'] - tol_y * (ptam_coords['y_max'] - ptam_coords['y_min'])
            corrected = True
	else:
	    y = ptam_coords['y']
        
	# Correct z
        if z < z_min - tolerance:
            msg += "Drone out of bounds: z<z_min {}".format(z)
            z = ptam_coords['z_min'] + tol_z * (ptam_coords['z_max'] - ptam_coords['z_min'])
            corrected = True
        elif z > z_max + tolerance:
            msg += "Drone out of bounds: z>z_max {}".format(z)
            z = ptam_coords['z_max'] - tol_z * (ptam_coords['z_max'] - ptam_coords['z_min'])
            corrected = True
	else:
	    z = ptam_coords['z']

	# If position corrected, cancel trajectory and send goto valid position
        if ptam_coords['corrected_time'] > -1 and rospy.get_time() - ptam_coords['corrected_time'] < time_between_corrections:
	    # This is to prevent sending the corrected goto commands at 100Hz
            pass
        elif corrected:
            ptam_coords['corrected_time'] = rospy.get_time()
            ptam_coords['out_of_limits'] = True
	    pub.publish("c clearCommands")
            rospy.loginfo("Corrected.\n{}".format(msg))

            # Send the right goto destination in PTAM coordinates
            pub.publish("c goto {} {} {} 0".format(x, y, z))
        else:
            ptam_coords['corrected_time'] = -1
            ptam_coords['out_of_limits'] = False
    
    def callback_pose_ptam(data):
        # Keep track of the current drone PTAM coordinates
	# Here we also invert x and y to transform from gazebo to ptam coordinates
        ptam_coords['x'] = data.y
        ptam_coords['y'] = data.x
        ptam_coords['z'] = data.z
        # First time: initialize min and max in PTAM coordinates
        if ptam_coords['init']:
	    ptam_coords['init'] = False
	    ptam_coords['x_min'] = ptam_coords['x']
	    ptam_coords['x_max'] = ptam_coords['x']
	    ptam_coords['y_min'] = ptam_coords['y']
	    ptam_coords['y_max'] = ptam_coords['y']
	    ptam_coords['z_min'] = ptam_coords['z']
	    ptam_coords['z_max'] = ptam_coords['z']
        # If drone is in restricted area, keep track of min and max of each coordinate (in PTAM coordinates)
        elif not ptam_coords['out_of_limits']:
	    if ptam_coords['x'] < ptam_coords['x_min']:
	        ptam_coords['x_min'] = ptam_coords['x']
                rospy.loginfo('Updated xmin: {}'.format(ptam_coords['x']))
	    if ptam_coords['x'] > ptam_coords['x_max']:
	        ptam_coords['x_max'] = ptam_coords['x']
                rospy.loginfo('Updated xmax: {}'.format(ptam_coords['x']))
	    if ptam_coords['y'] < ptam_coords['y_min']:
	        ptam_coords['y_min'] = ptam_coords['y']
                rospy.loginfo('Updated ymin: {}'.format(ptam_coords['y']))
	    if ptam_coords['y'] > ptam_coords['y_max']:
	        ptam_coords['y_max'] = ptam_coords['y']
                rospy.loginfo('Updated ymax: {}'.format(ptam_coords['y']))
	    if ptam_coords['z'] <  ptam_coords['z_min']:
	        ptam_coords['z_min'] = ptam_coords['z']
                rospy.loginfo('Updated zmin: {}'.format(ptam_coords['z']))
	    if ptam_coords['z'] > ptam_coords['z_max']:
	        ptam_coords['z_max'] = ptam_coords['z']
                rospy.loginfo('Updated zmax: {}'.format(ptam_coords['z']))

    rospy.Subscriber("/ardrone/predictedPose", filter_state, callback_pose_ptam)
    rospy.Subscriber("/ground_truth/state", Odometry, callback_pose)

    rospy.spin()

if __name__ == '__main__':
    try:
        if len(sys.argv) > 8 :
            print("usage: env_limits.py [x_min x_max y_min y_max z_min z_max tolerance]\n\tDefault values are: 0, 5, 0, 5, 1, 5, 0.5")
        elif len(sys.argv) == 1:
           env_limits_node()
        else:
           env_limits_node(float(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), float(sys.argv[7]))
    except rospy.ROSInterruptException:
        pass
    except IndexError:
            print("usage: env_limits.py [x_min x_max y_min y_max z_min z_max tolerance]\n\tDefault values are: 0, 5, 0, 5, 1, 5, 0.5")
    except ValueError:
        print("Error: Arguments need to be numbers.")
