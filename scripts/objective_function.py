#!/usr/bin/env python
# license removed for brevity
import rospy
import math
import random
import sys
from tum_ardrone.msg import filter_state
from nav_msgs.msg import Odometry
from std_msgs.msg import String
from std_msgs.msg import Float64

def objective_fn_node(mu_n=0, sig_n=0.02, x_min=0, x_max=5, y_min=0, y_max=5, z_min=0, z_max=5):
    # Setup publisher
    pub_gt = rospy.Publisher('/ardrone_CSBO/objective_fn/ground_trurh', Float64, queue_size=1)
    pub_noisy = rospy.Publisher('/ardrone_CSBO/objective_fn/noisy', Float64, queue_size=1)
    rospy.init_node('objective_function', anonymous=True)
    
    # Sleep a little be to make sure the publisher is registered
    rospy.sleep(0.3)
    rospy.loginfo('Ready and listening.')
    rate = rospy.Rate(10)    

    # Callback for ground truth Pose topic. Computes the objective function for each message received here.
    def callback_pose(data):
        position = data.pose.pose.position
        x = float(position.x)
        y = float(position.y)
        z = float(position.z)
        if x < x_min or x > x_max or y < y_min or y > y_max or z < z_min or z > z_max:
            val = -1000
        else:
            val = obj_fn(float(position.x), float(position.y), float(position.z))
        pub_gt.publish(val)
        pub_noisy.publish(val + random.normalvariate(mu_n, sig_n))

    rospy.Subscriber("/ground_truth/state", Odometry, callback_pose)
    rospy.spin()

# Using the objective function from Roman's paper. Does not depend on z.
def obj_fn(x, y, z):
    return math.exp(-math.pow(x - 4, 2)) * math.exp(-math.pow(y - 1, 2)) + 0.8 * math.exp(
            -math.pow(x - 1, 2)) * math.exp(-math.pow((y - 4) / 2.5, 2)) + 4 * math.exp(
            -math.pow((x - 10) / 5, 2)) * math.exp(-math.pow((y - 10) / 5, 2))

if __name__ == '__main__':
    arg_offset = 1
    while arg_offset < len(sys.argv) and sys.argv[arg_offset].startswith("__"):
        arg_offset += 1
    real_arg_len = len(sys.argv) - arg_offset
    try:
        if real_arg_len > 2 :
            print("usage: objective_function.py [mu_noise sig_noise]")
        elif real_arg_len == 0:
           objective_fn_node()
        else:
           objective_fn_node(float(sys.argv[arg_offset]), float(sys.argv[arg_offset + 1]))
    except rospy.ROSInterruptException:
        pass
    except IndexError:
            print("usage: objective_function.py [mu_noise sig_noise]")
    except ValueError:
        print("Error: Arguments '{}' need to be numbers.".format(', '.join(sys.argv[arg_offset:])))
