# ARDrone CSBO

### Description
Continuous Sequential Bayesian Optimisation for Parrot ARDrone 2.0 with PTAM localisation. Simulation done using tum_simulator (Gazebo).

### Installation
- Clone this repository.
- Download  project 'cont_act_pomdp' at https://philippeMorere@bitbucket.org/philippeMorere/cont_act_pomdp.git in ardrone_CSBO root directory.
- run `rosmake ardrone_CSBO`. You might need to add project to ROS_PACKAGE_PATH
- run `sudo pip install GPy` (possibly `sudo apt-get install gfortran`)
- run `sudo apt-get install python-nlopt`
- Create a ros catking workspace and add `ardrone_autonomy`, `tum_ardrone` and `tum_simulator` to the `src` folder. (Links: https://github.com/AutonomyLab/ardrone_autonomy.git, https://github.com/tum-vision/tum_ardrone.git, https://github.com/occomco/tum_simulator.git / tum simulator for Hydro: https://github.com/tum-vision/tum_simulator/tree/master)
- copy file `ardrone_CSBO/misc/ardrone_testworld.world` to `<catkin_workspace>/src/tum_simulator/cvg_sim_gazebo/worlds`.
- run `catkin_make` from catkin workspace root directory.

### Usage
Run `roslaunch ardrone_CSBO simulatiors.launch`.

### Troubleshooting
- Tum_ardrone sometimes behaves in a strange way after autoInit. The estimated pose is completely off and the drone oscillates like it has a bad PID controller. The problem might come from the PTAM positioning.
- Gazebo GUI sometimes crashes at startup. However the GUI is not needed for the algorithm to run. 
